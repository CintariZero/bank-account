package oops.bank;

public class Checking extends Account {
	private double overdraft;
	
	public Checking() {
		this("Unknown", 0);
	}

	public Checking(String holderName, double balance) {
		super(holderName, balance);
		overdraft = OD_LIMIT;
	}
	public void deposit(double amount)
	{
		if (overdraft < OD_LIMIT)
		{
			overdraft += amount;
			if(overdraft > OD_LIMIT)
			{
				balance += overdraft - 200;
				overdraft = OD_LIMIT;
			}
		}
		else
			balance += amount;
	}
	public void withdraw(double amount)
	{
		if (amount > balance){
			if (amount <= overdraft)
			{
				overdraft -= amount;
				System.out.println("Insufficient funds! But your overdraft amount has not been exceeded, your new overdraft amount is: " + overdraft);
			}
			else
				System.out.println("Insufficient funds, and this withdrawl exceeds your overdraft balance!");
			}
		else
			balance -= amount;
			Transaction txn = new Transaction("Dr", amount, balance);
			LogFile.Log(acntNo, txn);
			txns[idx++] = txn;
	}
	public static void main(String[] args) {
		Checking c = new Checking("Dave", 300);
		c.withdraw(0);
		
	}
}
