package oops.bank;


public class Savings extends Account {
	private double  minBalance = MIN_SAV_BAL;
	public Savings() 
	{
		this("Unknown", 0);
	}

	public Savings(String holderName, double balance) {
		super(holderName, balance);
		
	}
	public void withdraw(double amount) throws BalanceException
	{
		HandleBalance exObj = new HandleBalance();
		if(amount <= balance)
		{
			if ((balance-amount) < minBalance)
			{
				try 
				{
					throw new BalanceException();
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				exObj.throwBalanceException();
			}
			else
			{
				balance -= amount;
				Transaction txn = new Transaction("Dr", amount, balance);
				LogFile.Log(acntNo, txn);
				txns[idx++] = txn;
			}	
		}
		else
		{
			System.out.println("Insufficient funds!");
		}
	}
}
