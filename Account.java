package oops.bank;

public abstract class Account implements IBank 
{
	protected int acntNo;
	private String holderName;
	protected double balance;
	protected Transaction[] txns;
	protected int idx;
	static int defAcntNo = INIT_ACCONT_NO;
	public Account() {
		this("Unknown",0);
		defAcntNo++;
	}
	public Account(String holderName, double balance) {
		this.acntNo = defAcntNo;
		this.holderName = holderName;
		this.balance = balance;
		//
		Transaction txn = new Transaction("OB", balance, balance);
		LogFile.Log(this.acntNo, txn);
		txns = new Transaction[5];
		txns[idx++] = txn;
		defAcntNo++;
	}
	public void summary()
	{
		System.out.println("Account number: " + acntNo);
		System.out.println("Holder name: " + holderName);
		System.out.println("Balance: " + balance);
	}
	public void deposit(double amount)
	{
		balance += amount;
		Transaction txn = new Transaction("Cr", amount, balance);
		LogFile.Log(acntNo, txn);
		txns[idx++] = txn;
	}
	public void withdraw(double amount) throws BalanceException
	{
		if (amount > balance)
			System.out.println("Insufficient funds!");
		else
		{
			balance -= amount;
			Transaction txn = new Transaction("Cr", amount, balance);
			LogFile.Log(acntNo, txn);
			txns[idx++] = txn;
		}
	}
	public void statement(){
		System.out.println("Type\nAmount\nBalance");
		for (int i=0;i<idx;i++)
		{
			System.out.println(txns[i]);
		}
	}
}
