package oops.bank;

public class Transaction
{
	private int accountNum;
	private String txnType;
	private double txnAmount;
	private double balance;
	public Transaction(String txnType, double txnAmount, double balance)
	{
		this.txnType = txnType;
		this.txnAmount = txnAmount;
		this.balance = balance;
	}
	public String toString()
	{
		String tran = "Transaction type " + txnType + "\nAmount: " + txnAmount + "\nBalance: " + balance;
		return tran;
	}
	public String logString()
	{
		String tran = " " + txnType + " " + txnAmount;
		return tran;
	}
	
}
