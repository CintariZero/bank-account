package oops.bank;

public interface IBank {
	void summary();
	void deposit(double amount);
	void withdraw(double amount) throws BalanceException;
	void statement();
	
	double OD_LIMIT = 200;
	double MIN_SAV_BAL = 50;
	int INIT_ACCONT_NO = 1000;
}
