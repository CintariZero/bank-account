package oops.bank;

public final class AccountFactory {
	private AccountFactory()
	{
		
	}
	public static IBank createAccount(String holder, double balance, String acType)
	{
		IBank ref = null;
		if (acType.equalsIgnoreCase("savings")){
			ref = new Savings(holder, balance);
		}
		else
		{
			ref = new Checking(holder, balance);
		}
		return ref;	
	}
	public static void main(String[] args) throws BalanceException {
		IBank robBank = createAccount("Rob",100,"savings");
		robBank.deposit(100);
		//robBank.summary();
		System.out.println("Withdrawl 1: ");
		robBank.withdraw(50);
		robBank.deposit(70);
		//robBank.summary();
		//robBank.statement();
		
	}
}
