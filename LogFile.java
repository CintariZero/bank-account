package oops.bank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class LogFile
{
	public static void Log(int account, Transaction t)
	{
	    Calendar current = new GregorianCalendar();
	    current = Calendar.getInstance();
	    String date = current.get(Calendar.MONTH) + "-" + current.get(Calendar.DAY_OF_MONTH)+ "-" + current.get(Calendar.YEAR);
	    //System.out.println(date);
	    String filename = "src/oops/bank/";
	    filename += date += ".txt";
	    //System.out.println(filename);
	    String time = current.get(Calendar.HOUR_OF_DAY) + ":" + current.get(Calendar.MINUTE) + ":" + current.get(Calendar.MILLISECOND);
	    try {
	    	File f = new File(filename);
			FileWriter fw = new FileWriter(filename, f.exists());
			BufferedWriter bw = new BufferedWriter(fw);
			String log = account + t.logString() + " " + time;
			bw.write(log);
			bw.newLine();
			//writeYear(fw);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
