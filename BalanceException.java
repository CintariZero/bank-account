package oops.bank;

public class BalanceException extends Exception
{
	public static void printMessage()
	{
		System.out.println("Error: Your withdraw would have set your balance below the minimum, please select a lower balance.");
	}
	
}

